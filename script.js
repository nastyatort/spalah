//в обьект добавлять только числа

function append(ob, x) {
    var len = ob.length;

    var type = typeof(x);
    if(type === 'number') {
        ob[len] = x;
        return ++ob.length;
    }else{
        return ob.length;
    }
}


var s = {'length': 0};
var l;

l = append(s, 5);
console.log('Length after 1 addition', l);
console.log(s);

l = append(s, 10);
console.log('Length after 2 addition', l);
console.log(s);

l = append(s, 'lklk');
console.log('Length after 3 addition', l);
console.log(s);

l = append(s, 15);
console.log('Length after 4 addition', l);
console.log(s);

console.log('\n------------------------\n');

//в обект добавлять только числа
//перед существующими элементами обьекта

function prepend(ob, x) {
    var len = ob.length;

    var type = typeof(x);
    if(type === 'number') {
        for(var i = len; i > 0; i--){
            ob[i] = ob[i - 1];
        }
        ob[len - len] = x;
        return ++ob.length;
    }else{
        return ob.length;
    }
}


l = prepend(s, 4);
console.log('Length after 1 addition', l);
console.log('State after 1 prepend', s, '\n');

l = prepend(s, 3);
console.log('Length after 2 addition', l);
console.log('State after 2 prepend', s, '\n');

l = prepend(s, 'lklk');
console.log('Length after 3 addition', l);
console.log('State after 3 prepend', s, '\n');

l = prepend(s, 2);
console.log('Length after 4 addition', l);
console.log('State after 4 prepend', s, '\n');

l = prepend(s, 1);
console.log('Length after 5 addition', l);
console.log('State after 5 prepend', s, '\n');


console.log('\n------------------------\n');

//удалить последний элемент и вернуть его значение

function take_from_end(ob) {
    var len = ob.length;
    var rem = ob[len - 1];
    delete ob[len - 1];
    --ob.length;
    return rem;
}



var removed;

console.log(s);

removed = take_from_end(s);
console.log('Removed:', removed, ' Object state:', s);

removed = take_from_end(s);
console.log('Removed:', removed, ' Object state:', s);


removed = take_from_end(s);
console.log('Removed:', removed, ' Object state:', s);

removed = take_from_end(s);
console.log('Removed:', removed, ' Object state:', s);


console.log('\n------------------------\n');


//удалить первый элемент и вернуть его значение


function take_from_start(ob) {
    var len = ob.length;
    var rem = ob[0];
    for(var i = 0; i < len; i++){
        ob[i] = ob[i + 1];
    }
    delete ob[len - 1];
    --ob.length;
    return rem;
}


var removedStart;

append(s, 4);
append(s, 5);

console.log(s);

removedStart = take_from_start(s);
console.log('Removed:', removedStart, ' Object state:', s);

removedStart = take_from_start(s);
console.log('Removed:', removedStart, ' Object state:', s);


removedStart = take_from_start(s);
console.log('Removed:', removedStart, ' Object state:', s);

removedStart = take_from_start(s);
console.log('Removed:', removedStart, ' Object state:', s);

removedStart = take_from_start(s);
console.log('Removed:', removedStart, ' Object state:', s);

console.log('\n------------------------\n');

//расположить элементы в порядке возрастания

append(s, 100);
append(s, 123123132);
append(s, 1);
append(s, 4);
append(s, -12312);

console.log(s);

function sort(ob) {
    var len = ob.length;

    for(var j = 0; j < len; j++){
        var help;
        for(var i = 0; i < len; i++){
            if(ob[i] > ob[i + 1]){
                help = ob[i];
                ob[i] = ob[i + 1];
                ob[i + 1] = help;
            }
        }
    }
}

sort(s);
console.log('sorted: ', s);
